#!/usr/bin/env bash

set -e

# ansible-galaxy install -r requirements.yml --force matomo

if [[ "${1}" != "" ]]; then
  ansible-playbook matomo_upgrade.yml --limit "${1}"
else
  ansible-playbook matomo_upgrade.yml
fi
