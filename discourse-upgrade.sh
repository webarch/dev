#!/usr/bin/env bash

# ansible-galaxy install -r requirements.yml --force

if [[ "${1}" != "" ]]; then
  ansible-playbook discourse_upgrade.yml -i hosts.yml -t discourse_upgrade --limit "${1}"
else
  ansible-playbook discourse_upgrade.yml -i hosts.yml -t discourse_upgrade
fi

