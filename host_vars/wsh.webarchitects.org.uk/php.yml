# CHANGES FROM https://git.coop/webarch/php/-/blob/main/defaults/main.yml
#
#             "opcache.jit": "tracing"
#             "opcache.jit_buffer_size": "41943040" # minimum size is 40961 bytes, 41943040 is 40 MB
#
---
php: true
php_check_legacy_variables: true
php_config:
  - name: PHP 8.4 configuration
    version: "8.4"
    state: present
    files:
      - name: PHP 8.4 JIT configuration
        path: /etc/php/8.4/mods-available/opcache.ini
        state: edited
        conf_absent:
          "opcache.jit": "0"
      - name: PHP 8.4 CLI configuration
        path: /etc/php/8.4/cli/php.ini
        state: present
        conf:
          apc:
            "apc.coredump_unmap": "0"
            "apc.enable_cli": "1"
            "apc.enabled": "1"
            "apc.entries_hint": "4096"
            "apc.gc_ttl": "3600"
            "apc.serializer": "php"
            "apc.shm_segments": "1"
            "apc.shm_size": "32M"
            "apc.slam_defense": "1"
            "apc.ttl": "0"
            "apc.use_request_time": "0"
          "CLI Server":
            "cli_server.color": "1"
          MySQLi:
            "mysqli.allow_local_infile": "0"
            "mysqli.allow_persistent": "1"
            "mysqli.default_port": '3306'
            "mysqli.max_links": "-1"
            "mysqli.max_persistent": "-1"
            "mysqli.reconnect": "0"
            "mysqli.default_socket": "/run/mysqld/mysqld.sock"
          opcache:
            "opcache.enable": "1"
            "opcache.enable_cli": "1"
            "opcache.interned_strings_buffer": "64"
            "opcache.jit": "tracing"
            "opcache.jit_buffer_size": "41943040"
            "opcache.max_accelerated_files": "100000"
            "opcache.memory_consumption": "1024"
            "opcache.revalidate_freq": "2"
            "opcache.use_cwd": "1"
            "opcache.validate_permission": "1"
            "opcache.validate_root": "1"
            "opcache.validate_timestamps": "1"
          Pdo_mysql:
            "pdo_mysql.default_socket": "/run/mysqld/mysqld.sock"
          PHP:
            "max_input_vars": 100000
      - name: PHP 8.4 FPM configuration
        path: /etc/php/8.4/fpm/php.ini
        state: edited
        conf:
          apc:
            "apc.enabled": "1"
            "apc.shm_size": "32M"
          Date:
            "date.timezone": "Europe/London"
          MySQLi:
            "mysqli.allow_local_infile": "0"
          opcache:
            "opcache.enable": "1"
            "opcache.interned_strings_buffer": "64"
            "opcache.jit": "tracing"
            "opcache.jit_buffer_size": "41943040"
            "opcache.max_accelerated_files": "100000"
            "opcache.memory_consumption": "1024"
            "opcache.revalidate_freq": "2"
            "opcache.use_cwd": "1"
            "opcache.validate_permission": "1"
            "opcache.validate_root": "1"
            "opcache.validate_timestamps": "1"
          PHP:
            allow_url_include: "0"
            default_socket_timeout: "600"
            disable_functions: "chgrp,chown,dl,pcntl_alarm,pcntl_fork,pcntl_waitpid,pcntl_wait,pcntl_wifexited,pcntl_wifstopped,pcntl_wifsignaled,pcntl_wifcontinued,pcntl_wexitstatus,pcntl_wtermsig,pcntl_wstopsig,pcntl_signal,pcntl_signal_get_handler,pcntl_signal_dispatch,pcntl_get_last_error,pcntl_strerror,pcntl_sigprocmask,pcntl_sigwaitinfo,pcntl_sigtimedwait,pcntl_exec,pcntl_getpriority,pcntl_setpriority,pcntl_async_signals,pcntl_unshare,posix_kill,posix_mkfifo,posix_setpgid,posix_setsid,posix_setuid"
            max_file_uploads: "60"
            max_execution_time: "600"
            max_input_nesting_level: "512"
            max_input_time: "600"
            max_input_vars: "100000"
            memory_limit: "512M"
            output_buffering: "4096"
            post_max_size: "512M"
            short_open_tag: "0"
            upload_max_filesize: "512M"
          Session:
            "session.save_path": "${TMPDIR}"
      - name: PHP 8.4 FPM www pool configuration
        path: /etc/php/8.4/fpm/pool.d/www.conf
        state: absent
      - name: PHP 8.4 FPM www84 pool configuration
        path: /etc/php/8.4/fpm/pool.d/www84.conf
        state: present
        conf:
          www84:
            "user": www-data
            "group": www-data
            "listen": /run/php/php8.4-fpm.sock
            "listen.owner": www-data
            "listen.group": www-data
            "pm": ondemand
            "pm.max_children": "4"
            "pm.process_idle_timeout": 10s
            "pm.status_path": /fpm84-status
  - name: PHP 8.3 configuration
    version: "8.3"
    state: present
    files:
      - name: PHP 8.3 JIT configuration
        path: /etc/php/8.3/mods-available/opcache.ini
        state: edited
        conf_absent:
          "opcache.jit": "0"
      - name: PHP 8.3 CLI configuration
        path: /etc/php/8.3/cli/php.ini
        state: present
        conf:
          apc:
            "apc.coredump_unmap": "0"
            "apc.enable_cli": "1"
            "apc.enabled": "1"
            "apc.entries_hint": "4096"
            "apc.gc_ttl": "3600"
            "apc.serializer": "php"
            "apc.shm_segments": "1"
            "apc.shm_size": "32M"
            "apc.slam_defense": "1"
            "apc.ttl": "0"
            "apc.use_request_time": "0"
          "CLI Server":
            "cli_server.color": "1"
          MySQLi:
            "mysqli.allow_local_infile": "0"
            "mysqli.allow_persistent": "1"
            "mysqli.default_port": '3306'
            "mysqli.max_links": "-1"
            "mysqli.max_persistent": "-1"
            "mysqli.reconnect": "0"
            "mysqli.default_socket": "/run/mysqld/mysqld.sock"
          opcache:
            "opcache.enable": "1"
            "opcache.enable_cli": "1"
            "opcache.interned_strings_buffer": "64"
            "opcache.jit": "tracing"
            "opcache.jit_buffer_size": "41943040"
            "opcache.max_accelerated_files": "100000"
            "opcache.memory_consumption": "1024"
            "opcache.revalidate_freq": "2"
            "opcache.use_cwd": "1"
            "opcache.validate_permission": "1"
            "opcache.validate_root": "1"
            "opcache.validate_timestamps": "1"
          Pdo_mysql:
            "pdo_mysql.default_socket": "/run/mysqld/mysqld.sock"
          PHP:
            "max_input_vars": 100000
      - name: PHP 8.3 FPM configuration
        path: /etc/php/8.3/fpm/php.ini
        state: edited
        conf:
          apc:
            "apc.enabled": "1"
            "apc.shm_size": "32M"
          Date:
            "date.timezone": "Europe/London"
          MySQLi:
            "mysqli.allow_local_infile": "0"
          opcache:
            "opcache.enable": "1"
            "opcache.interned_strings_buffer": "64"
            "opcache.jit": "tracing"
            "opcache.jit_buffer_size": "41943040" # minimum size is 40961 bytes
            "opcache.max_accelerated_files": "100000"
            "opcache.memory_consumption": "1024"
            "opcache.revalidate_freq": "2"
            "opcache.use_cwd": "1"
            "opcache.validate_permission": "1"
            "opcache.validate_root": "1"
            "opcache.validate_timestamps": "1"
          PHP:
            allow_url_include: "0"
            default_socket_timeout: "600"
            disable_functions: "chgrp,chown,dl,pcntl_alarm,pcntl_fork,pcntl_waitpid,pcntl_wait,pcntl_wifexited,pcntl_wifstopped,pcntl_wifsignaled,pcntl_wifcontinued,pcntl_wexitstatus,pcntl_wtermsig,pcntl_wstopsig,pcntl_signal,pcntl_signal_get_handler,pcntl_signal_dispatch,pcntl_get_last_error,pcntl_strerror,pcntl_sigprocmask,pcntl_sigwaitinfo,pcntl_sigtimedwait,pcntl_exec,pcntl_getpriority,pcntl_setpriority,pcntl_async_signals,pcntl_unshare,posix_kill,posix_mkfifo,posix_setpgid,posix_setsid,posix_setuid"
            max_file_uploads: "60"
            max_execution_time: "600"
            max_input_nesting_level: "512"
            max_input_time: "600"
            max_input_vars: "100000"
            memory_limit: "512M"
            output_buffering: "4096"
            post_max_size: "512M"
            short_open_tag: "0"
            upload_max_filesize: "512M"
          Session:
            "session.save_path": "${TMPDIR}"
      - name: PHP 8.3 FPM www pool configuration
        path: /etc/php/8.3/fpm/pool.d/www.conf
        state: absent
      - name: PHP 8.3 FPM www83 pool configuration
        path: /etc/php/8.3/fpm/pool.d/www83.conf
        state: present
        conf:
          www83:
            "user": www-data
            "group": www-data
            "listen": /run/php/php8.3-fpm.sock
            "listen.owner": www-data
            "listen.group": www-data
            "pm": ondemand
            "pm.max_children": "4"
            "pm.process_idle_timeout": 10s
            "pm.status_path": /fpm83-status
  - name: PHP 8.2 configuration
    version: "8.2"
    state: present
    files:
      - name: PHP 8.2 JIT configuration
        path: /etc/php/8.2/mods-available/opcache.ini
        state: edited
        conf_absent:
          "opcache.jit": "0"
      - name: PHP 8.2 CLI configuration
        path: /etc/php/8.2/cli/php.ini
        state: present
        conf:
          apc:
            "apc.coredump_unmap": "0"
            "apc.enable_cli": "1"
            "apc.enabled": "1"
            "apc.entries_hint": "4096"
            "apc.gc_ttl": "3600"
            "apc.serializer": "php"
            "apc.shm_segments": "1"
            "apc.shm_size": "32M"
            "apc.slam_defense": "1"
            "apc.ttl": "0"
            "apc.use_request_time": "0"
          "CLI Server":
            "cli_server.color": "1"
          MySQLi:
            "mysqli.allow_local_infile": "0"
            "mysqli.allow_persistent": "1"
            "mysqli.default_port": '3306'
            "mysqli.max_links": "-1"
            "mysqli.max_persistent": "-1"
            "mysqli.reconnect": "0"
            "mysqli.default_socket": "/run/mysqld/mysqld.sock"
          opcache:
            "opcache.enable": "1"
            "opcache.enable_cli": "1"
            "opcache.interned_strings_buffer": "64"
            "opcache.jit": "tracing"
            "opcache.jit_buffer_size": "41943040"
            "opcache.max_accelerated_files": "100000"
            "opcache.memory_consumption": "1024"
            "opcache.revalidate_freq": "2"
            "opcache.use_cwd": "1"
            "opcache.validate_permission": "1"
            "opcache.validate_root": "1"
            "opcache.validate_timestamps": "1"
          Pdo_mysql:
            "pdo_mysql.default_socket": "/run/mysqld/mysqld.sock"
          PHP:
            "max_input_vars": 100000
      - name: PHP 8.2 FPM configuration
        path: /etc/php/8.2/fpm/php.ini
        state: edited
        conf:
          apc:
            "apc.enabled": "1"
            "apc.shm_size": "32M"
          Date:
            "date.timezone": "Europe/London"
          MySQLi:
            "mysqli.allow_local_infile": "0"
          opcache:
            "opcache.enable": "1"
            "opcache.interned_strings_buffer": "64"
            "opcache.jit": "tracing"
            "opcache.jit_buffer_size": "41943040" # minimum size is 40961 bytes
            "opcache.max_accelerated_files": "100000"
            "opcache.memory_consumption": "1024"
            "opcache.revalidate_freq": "2"
            "opcache.use_cwd": "1"
            "opcache.validate_permission": "1"
            "opcache.validate_root": "1"
            "opcache.validate_timestamps": "1"
          PHP:
            allow_url_include: "0"
            default_socket_timeout: "600"
            disable_functions: "chgrp,chown,dl,pcntl_alarm,pcntl_fork,pcntl_waitpid,pcntl_wait,pcntl_wifexited,pcntl_wifstopped,pcntl_wifsignaled,pcntl_wifcontinued,pcntl_wexitstatus,pcntl_wtermsig,pcntl_wstopsig,pcntl_signal,pcntl_signal_get_handler,pcntl_signal_dispatch,pcntl_get_last_error,pcntl_strerror,pcntl_sigprocmask,pcntl_sigwaitinfo,pcntl_sigtimedwait,pcntl_exec,pcntl_getpriority,pcntl_setpriority,pcntl_async_signals,pcntl_unshare,posix_kill,posix_mkfifo,posix_setpgid,posix_setsid,posix_setuid"
            max_file_uploads: "60"
            max_execution_time: "600"
            max_input_nesting_level: "512"
            max_input_time: "600"
            max_input_vars: "100000"
            memory_limit: "512M"
            output_buffering: "4096"
            post_max_size: "512M"
            short_open_tag: "0"
            upload_max_filesize: "512M"
          Session:
            "session.save_path": "${TMPDIR}"
      - name: PHP 8.2 FPM www pool configuration
        path: /etc/php/8.2/fpm/pool.d/www.conf
        state: absent
      - name: PHP 8.2 FPM www82 pool configuration
        path: /etc/php/8.2/fpm/pool.d/www82.conf
        state: present
        conf:
          www82:
            "user": www-data
            "group": www-data
            "listen": /run/php/php8.2-fpm.sock
            "listen.owner": www-data
            "listen.group": www-data
            "pm": ondemand
            "pm.max_children": "4"
            "pm.process_idle_timeout": 10s
            "pm.status_path": /fpm82-status
  - name: PHP 8.1 configuration
    version: "8.1"
    state: present
    files:
      - name: PHP 8.1 JIT configuration
        path: /etc/php/8.1/mods-available/opcache.ini
        state: edited
        conf_absent:
          "opcache.jit": "0"
      - name: PHP 8.1 CLI configuration
        path: /etc/php/8.1/cli/php.ini
        state: present
        conf:
          apc:
            "apc.coredump_unmap": "0"
            "apc.enable_cli": "1"
            "apc.enabled": "1"
            "apc.entries_hint": "4096"
            "apc.gc_ttl": "3600"
            "apc.serializer": "php"
            "apc.shm_segments": "1"
            "apc.shm_size": "32M"
            "apc.slam_defense": "1"
            "apc.ttl": "0"
            "apc.use_request_time": "0"
          "CLI Server":
            "cli_server.color": "1"
          MySQLi:
            "mysqli.allow_local_infile": "0"
            "mysqli.allow_persistent": "1"
            "mysqli.default_port": '3306'
            "mysqli.max_links": "-1"
            "mysqli.max_persistent": "-1"
            "mysqli.reconnect": "0"
            "mysqli.default_socket": "/run/mysqld/mysqld.sock"
          opcache:
            "opcache.enable": "1"
            "opcache.enable_cli": "1"
            "opcache.interned_strings_buffer": "64"
            "opcache.jit": "tracing"
            "opcache.jit_buffer_size": "41943040"
            "opcache.max_accelerated_files": "100000"
            "opcache.memory_consumption": "1024"
            "opcache.revalidate_freq": "2"
            "opcache.use_cwd": "1"
            "opcache.validate_permission": "1"
            "opcache.validate_root": "1"
            "opcache.validate_timestamps": "1"
          Pdo_mysql:
            "pdo_mysql.default_socket": "/run/mysqld/mysqld.sock"
          PHP:
            "max_input_vars": 100000
      - name: PHP 8.1 FPM configuration
        path: /etc/php/8.1/fpm/php.ini
        state: edited
        conf:
          apc:
            "apc.enabled": "1"
            "apc.shm_size": "32M"
          Date:
            "date.timezone": "Europe/London"
          MySQLi:
            "mysqli.allow_local_infile": "0"
          opcache:
            "opcache.enable": "1"
            "opcache.interned_strings_buffer": "64"
            "opcache.jit": "tracing"
            "opcache.jit_buffer_size": "41943040" # minimum size is 40961 bytes
            "opcache.max_accelerated_files": "100000"
            "opcache.memory_consumption": "1024"
            "opcache.revalidate_freq": "2"
            "opcache.use_cwd": "1"
            "opcache.validate_permission": "1"
            "opcache.validate_root": "1"
            "opcache.validate_timestamps": "1"
          PHP:
            allow_url_include: "0"
            default_socket_timeout: "600"
            disable_functions: "chgrp,chown,dl,pcntl_alarm,pcntl_fork,pcntl_waitpid,pcntl_wait,pcntl_wifexited,pcntl_wifstopped,pcntl_wifsignaled,pcntl_wifcontinued,pcntl_wexitstatus,pcntl_wtermsig,pcntl_wstopsig,pcntl_signal,pcntl_signal_get_handler,pcntl_signal_dispatch,pcntl_get_last_error,pcntl_strerror,pcntl_sigprocmask,pcntl_sigwaitinfo,pcntl_sigtimedwait,pcntl_exec,pcntl_getpriority,pcntl_setpriority,pcntl_async_signals,pcntl_unshare,posix_kill,posix_mkfifo,posix_setpgid,posix_setsid,posix_setuid"
            max_file_uploads: "60"
            max_execution_time: "600"
            max_input_nesting_level: "512"
            max_input_time: "600"
            max_input_vars: "100000"
            memory_limit: "512M"
            output_buffering: "4096"
            post_max_size: "512M"
            short_open_tag: "0"
            upload_max_filesize: "512M"
          Session:
            "session.save_path": "${TMPDIR}"
      - name: PHP 8.1 FPM www pool configuration
        path: /etc/php/8.1/fpm/pool.d/www.conf
        state: absent
      - name: PHP 8.1 FPM www81 pool configuration
        path: /etc/php/8.1/fpm/pool.d/www81.conf
        state: present
        conf:
          www81:
            "user": www-data
            "group": www-data
            "listen": /run/php/php8.1-fpm.sock
            "listen.owner": www-data
            "listen.group": www-data
            "pm": ondemand
            "pm.max_children": "4"
            "pm.process_idle_timeout": 10s
            "pm.status_path": /fpm81-status
  - name: PHP 8.0 configuration
    version: "8.0"
    state: present
    files:
      - name: PHP 8.0 JIT configuration
        path: /etc/php/8.0/mods-available/opcache.ini
        state: edited
        conf_absent:
          "opcache.jit": "0"
      - name: PHP 8.0 CLI configuration
        path: /etc/php/8.0/cli/php.ini
        state: present
        conf:
          apc:
            "apc.coredump_unmap": "0"
            "apc.enable_cli": "1"
            "apc.enabled": "1"
            "apc.entries_hint": "4096"
            "apc.gc_ttl": "3600"
            "apc.serializer": "php"
            "apc.shm_segments": "1"
            "apc.shm_size": "32M"
            "apc.slam_defense": "1"
            "apc.ttl": "0"
            "apc.use_request_time": "0"
          "CLI Server":
            "cli_server.color": "1"
          MySQLi:
            "mysqli.allow_local_infile": "0"
            "mysqli.allow_persistent": "1"
            "mysqli.default_port": '3306'
            "mysqli.max_links": "-1"
            "mysqli.max_persistent": "-1"
            "mysqli.reconnect": "0"
            "mysqli.default_socket": "/run/mysqld/mysqld.sock"
          opcache:
            "opcache.enable": "1"
            "opcache.enable_cli": "1"
            "opcache.interned_strings_buffer": "64"
            "opcache.jit": "tracing"
            "opcache.jit_buffer_size": "41943040"
            "opcache.max_accelerated_files": "100000"
            "opcache.memory_consumption": "1024"
            "opcache.revalidate_freq": "2"
            "opcache.use_cwd": "1"
            "opcache.validate_permission": "1"
            "opcache.validate_root": "1"
            "opcache.validate_timestamps": "1"
          Pdo_mysql:
            "pdo_mysql.default_socket": "/run/mysqld/mysqld.sock"
          PHP:
            "max_input_vars": 100000
      - name: PHP 8.0 FPM configuration
        path: /etc/php/8.0/fpm/php.ini
        state: edited
        conf:
          apc:
            "apc.enabled": "1"
            "apc.shm_size": "32M"
          Date:
            "date.timezone": "Europe/London"
          MySQLi:
            "mysqli.allow_local_infile": "0"
          opcache:
            "opcache.enable": "1"
            "opcache.interned_strings_buffer": "64"
            "opcache.jit": "tracing"
            "opcache.jit_buffer_size": "41943040" # minimum size is 40961 bytes
            "opcache.max_accelerated_files": "100000"
            "opcache.memory_consumption": "1024"
            "opcache.revalidate_freq": "2"
            "opcache.use_cwd": "1"
            "opcache.validate_permission": "1"
            "opcache.validate_root": "1"
            "opcache.validate_timestamps": "1"
          PHP:
            allow_url_include: "0"
            default_socket_timeout: "600"
            disable_functions: "chgrp,chown,dl,pcntl_alarm,pcntl_fork,pcntl_waitpid,pcntl_wait,pcntl_wifexited,pcntl_wifstopped,pcntl_wifsignaled,pcntl_wifcontinued,pcntl_wexitstatus,pcntl_wtermsig,pcntl_wstopsig,pcntl_signal,pcntl_signal_get_handler,pcntl_signal_dispatch,pcntl_get_last_error,pcntl_strerror,pcntl_sigprocmask,pcntl_sigwaitinfo,pcntl_sigtimedwait,pcntl_exec,pcntl_getpriority,pcntl_setpriority,pcntl_async_signals,pcntl_unshare,posix_kill,posix_mkfifo,posix_setpgid,posix_setsid,posix_setuid"
            max_file_uploads: "60"
            max_execution_time: "600"
            max_input_nesting_level: "512"
            max_input_time: "600"
            max_input_vars: "100000"
            memory_limit: "512M"
            output_buffering: "4096"
            post_max_size: "512M"
            short_open_tag: "0"
            upload_max_filesize: "512M"
          Session:
            "session.save_path": "${TMPDIR}"
      - name: PHP 8.0 FPM www pool configuration
        path: /etc/php/8.0/fpm/pool.d/www.conf
        state: absent
      - name: PHP 8.0 FPM www81 pool configuration
        path: /etc/php/8.0/fpm/pool.d/www80.conf
        state: present
        conf:
          www80:
            "user": www-data
            "group": www-data
            "listen": /run/php/php8.0-fpm.sock
            "listen.owner": www-data
            "listen.group": www-data
            "pm": ondemand
            "pm.max_children": "4"
            "pm.process_idle_timeout": 10s
            "pm.status_path": /fpm80-status
  - name: PHP 7.4 configuration
    version: "7.4"
    state: present
    files:
      - name: PHP 7.4 CLI configuration
        path: /etc/php/7.4/cli/php.ini
        state: present
        conf:
          apc:
            "apc.coredump_unmap": "0"
            "apc.enable_cli": "1"
            "apc.enabled": "1"
            "apc.entries_hint": "4096"
            "apc.gc_ttl": "3600"
            "apc.serializer": "php"
            "apc.shm_segments": "1"
            "apc.shm_size": "32M"
            "apc.slam_defense": "1"
            "apc.ttl": "0"
            "apc.use_request_time": "0"
          "CLI Server":
            "cli_server.color": "1"
          MySQLi:
            "mysqli.allow_local_infile": "0"
            "mysqli.allow_persistent": "1"
            "mysqli.default_port": '3306'
            "mysqli.max_links": "-1"
            "mysqli.max_persistent": "-1"
            "mysqli.reconnect": "0"
            "mysqli.default_socket": "/run/mysqld/mysqld.sock"
          opcache:
            "opcache.enable": "1"
            "opcache.enable_cli": "1"
            "opcache.interned_strings_buffer": "64"
            "opcache.max_accelerated_files": "100000"
            "opcache.memory_consumption": "1024"
            "opcache.revalidate_freq": "2"
            "opcache.use_cwd": "1"
            "opcache.validate_permission": "1"
            "opcache.validate_root": "1"
            "opcache.validate_timestamps": "1"
          Pdo_mysql:
            "pdo_mysql.default_socket": "/run/mysqld/mysqld.sock"
          PHP:
            "max_input_vars": 100000
      - name: PHP 7.4 FPM configuration
        path: /etc/php/7.4/fpm/php.ini
        state: edited
        conf:
          apc:
            "apc.enabled": "1"
            "apc.shm_size": "32M"
          Date:
            "date.timezone": "Europe/London"
          MySQLi:
            "mysqli.allow_local_infile": "0"
          opcache:
            "opcache.enable": "1"
            "opcache.interned_strings_buffer": "64"
            "opcache.max_accelerated_files": "100000"
            "opcache.memory_consumption": "1024"
            "opcache.revalidate_freq": "2"
            "opcache.use_cwd": "1"
            "opcache.validate_permission": "1"
            "opcache.validate_root": "1"
            "opcache.validate_timestamps": "1"
          PHP:
            allow_url_include: "0"
            default_socket_timeout: "600"
            disable_functions: "chgrp,chown,dl,pcntl_alarm,pcntl_fork,pcntl_waitpid,pcntl_wait,pcntl_wifexited,pcntl_wifstopped,pcntl_wifsignaled,pcntl_wifcontinued,pcntl_wexitstatus,pcntl_wtermsig,pcntl_wstopsig,pcntl_signal,pcntl_signal_get_handler,pcntl_signal_dispatch,pcntl_get_last_error,pcntl_strerror,pcntl_sigprocmask,pcntl_sigwaitinfo,pcntl_sigtimedwait,pcntl_exec,pcntl_getpriority,pcntl_setpriority,pcntl_async_signals,pcntl_unshare,posix_kill,posix_mkfifo,posix_setpgid,posix_setsid,posix_setuid"
            max_file_uploads: "60"
            max_execution_time: "600"
            max_input_nesting_level: "512"
            max_input_time: "600"
            max_input_vars: "100000"
            memory_limit: "512M"
            output_buffering: "4096"
            post_max_size: "512M"
            short_open_tag: "0"
            upload_max_filesize: "512M"
          Session:
            "session.save_path": "${TMPDIR}"
      - name: PHP 7.4 FPM www pool configuration
        path: /etc/php/7.4/fpm/pool.d/www.conf
        state: absent
      - name: PHP 7.4 FPM www74 pool configuration
        path: /etc/php/7.4/fpm/pool.d/www74.conf
        state: present
        conf:
          www74:
            "user": www-data
            "group": www-data
            "listen": /run/php/php7.4-fpm.sock
            "listen.owner": www-data
            "listen.group": www-data
            "pm": ondemand
            "pm.max_children": "4"
            "pm.process_idle_timeout": 10s
            "pm.status_path": /fpm74-status
...
