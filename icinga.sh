#!/usr/bin/env bash

if [[ "${2}" != "" ]]; then
  # ansible-galaxy install -r requirements.yml --force && \
    ansible-playbook icinga.yml --limit "${1},icinga_master_nodes" --tags "${2}" --diff
elif [[ "${1}" != "" ]]; then
  # ansible-galaxy install -r requirements.yml --force icinga && \
    ansible-playbook icinga.yml --limit "${1},icinga_master_nodes" -t icinga --diff
else
  # ansible-galaxy install -r requirements.yml --force && \
    ansible-playbook icinga.yml --diff
fi

