# Webarchitects Development Servers Ansible

The repo contains the Ansible used to configure the Webarchitects development servers.

## Copyright

Copyright 2024 Chris Croome, &lt;[chris@webarchitects.co.uk](mailto:chris@webarchitects.co.uk)&gt;.

This role is released under [the same terms as Ansible itself](https://github.com/ansible/ansible/blob/devel/COPYING), the [GNU GPLv3](LICENSE).
